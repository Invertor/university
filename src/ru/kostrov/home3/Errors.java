package ru.kostrov.home3;

public class Errors {
    static void nullPointerErr() {
        Integer[] a = new Integer[4];
        String s;
        s = a[0].toString();
    }

    static void arrayErr() {
        int[] a = new int[]{1, 2, 3};
        int i;
        i = a[4];
    }

    static void throwErr() {
        throw new ArithmeticException("User throw");
    }

    static void throwsErr() throws MyException {
        throw new MyException("User throw");
    }
    private static final int inCase = 4;
    public static void main(String[] args) {

        switch (inCase) {
            case 1:
                nullPointerErr();
                break;
            case 2:
                arrayErr();
                break;
            case 3:
                throwErr();
            case 4:
                try {
                    throwsErr();
                } catch (MyException ex) {
                    System.out.println(ex.getMessage());
                }
                break;
        }
    }

    static class MyException extends Exception {
        MyException() {
            super();
        }

        MyException(String message) {
            super(message);
        }

        MyException(String message, Throwable cause) {
            super(message, cause);
        }

        MyException(Throwable cause) {
            super(cause);
        }

    }
}

