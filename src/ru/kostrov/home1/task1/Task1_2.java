package ru.kostrov.home1.task1;

import java.util.Scanner;

public class Task1_2 {
    private static final int NALOG = 13;
    public static void main(String[] args) {
        float zarplata, zarplataNaRuki;
        if ( args.length > 0 ) {
            Scanner scanner = new Scanner(args[0]);
            if ( scanner.hasNextInt() ) {
                zarplata = scanner.nextInt();
                zarplataNaRuki = zarplata * ( 1.0f - ( NALOG  / 100.0f )) ;
                System.out.printf("%.2f руб.\n", zarplataNaRuki);
            }
        }

    }
}
