package ru.kostrov.home1.task1;

import java.util.Scanner;

public class Task1_3 {
    public static void main(String[] args) {
        int minutes, hours;
        String hoursName = "";
        if (args.length > 0) {
            Scanner scanner = new Scanner(args[0]);
            if (scanner.hasNextInt()) {
                minutes = scanner.nextInt();
                hours = minutes / 60;
                switch (Math.floorMod(hours, 10)) {
                    case 1:
                        hoursName = "час";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        hoursName = "часа";
                        break;
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 0:
                        hoursName = "часов";
                        break;
                }

                System.out.printf( "%d %s\n", hours, hoursName );
            }
        }
    }
}
