package ru.kostrov.home1.task1;

import java.util.Scanner;

public class Task1_1 {
    private static final float PRICE = 43f;
    public static void main(String[] args) {
        float liters, sum;
        if ( args.length > 0 ) {
            Scanner scanner = new Scanner(args[0]);
            if ( scanner.hasNextInt() ) {
                liters = scanner.nextInt();
                sum = liters * PRICE;
                System.out.printf("%.0f руб.\n", Math.floor( sum ));
            }
        }
    }
}
