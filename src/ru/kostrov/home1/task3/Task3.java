package ru.kostrov.home1.task3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        int x, maxNum;
        if (args.length > 0) {
            Scanner scanner = new Scanner(args[0]);
            if (scanner.hasNextInt()) {
                maxNum = scanner.nextInt();
                for (int i = 1; i <= 10; i++) {
                    for (int j = 1; j <= 10; j++) {
                        x = i * j;
                        if (x <= maxNum) {
                            System.out.printf("%d*%d=%d\t", j, i, x);
                        }
                    }
                    System.out.println();
                }
            }
        }
    }
}
