package ru.kostrov.home1.task4;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        long n, nFact = 0;
        if (args.length > 0) {
            Scanner scanner = new Scanner(args[0]);
            if (scanner.hasNextInt()) {
                n = scanner.nextInt();
                if (n >= 0) {
                    try {
                        nFact = Factorial(n);
                    } catch (ArithmeticException overflowException) {
                        System.out.println(overflowException.getLocalizedMessage());
                    }
                    if(nFact>0) {
                        System.out.println(nFact);
                    }
                }
            }
        }
    }

    static long Factorial(long n) throws ArithmeticException {
        //выдаем только правильные значения
        long nFact;
        if (n >= 0) {
            nFact = 1;
            for (int i = 2; i <= n; i++) {
                nFact = Math.multiplyExact(nFact, i);
            }
            return nFact;
        }else{
            throw new ArithmeticException("calc error");
        }
    }
}
