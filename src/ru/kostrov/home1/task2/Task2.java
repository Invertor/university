package ru.kostrov.home1.task2;

import java.util.Scanner;

public class Task2 {
    private static final int N = 4;

    public static void main(String[] args) {
        int num[] = new int[N];
        int minNum;
        boolean validNum = true;
        if (args.length >= N) {
            for (int i = 0; i < N; i++) {
                Scanner scanner = new Scanner(args[i]);
                if (scanner.hasNextInt()) {
                    num[i] = scanner.nextInt();
                } else {
                    validNum = false;
                }
            }
        } else {
            validNum = false;
        }
        if (validNum) {
            minNum = num[0];
            for (int i = 1; i < N; i++) {
                minNum = Math.min(minNum, num[i]);
            }
            System.out.println(minNum);
        }
    }


}
