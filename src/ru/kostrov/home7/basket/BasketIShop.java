package ru.kostrov.home7.basket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BasketIShop implements Basket {
    HashMap<String,Integer> products;

    public  BasketIShop(){
        products=new HashMap<>();
    }

    @Override
    public void addProduct(String product, int quantity) {
        if (products.containsKey(product)) {
            quantity += products.get(product);
            products.replace(product, quantity);
        }else{
            products.put(product,quantity);
        }
    }

    @Override
    public void removeProduct(String product) {
        products.remove(product);
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        if (products.containsKey(product)) {
            quantity += products.get(product);
            products.replace(product, quantity);
        }
    }

    @Override
    public void clear() {
        products.clear();
    }

    @Override
    public List<String> getProduct() {
        return new ArrayList(products.keySet());
    }

    @Override
    public int getProductQuantity(String product) {
        if (products.containsKey(product)) {
            return products.get(product);
        }else{
            return 0;
        }
    }
}
