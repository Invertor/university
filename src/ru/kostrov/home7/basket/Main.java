package ru.kostrov.home7.basket;

public class Main {
    public static void main(String[] args) {
        BasketIShop bis = new BasketIShop();
        System.out.println( bis.getProduct());
        System.out.println(bis.getProductQuantity("Banana"));
        bis.addProduct("Banana",1);
        bis.addProduct("Nuts",10);
        bis.updateProductQuantity("Cherry",2);
        System.out.println( bis.getProduct());
        System.out.println(bis.getProductQuantity("Banana"));
        bis.updateProductQuantity("Banana",2);
        bis.addProduct("Cherry",1);
        bis.removeProduct("Nuts");
        System.out.println( bis.getProduct());
        System.out.println(bis.getProductQuantity("Banana"));
        bis.clear();
        System.out.println( bis.getProduct());
        bis.removeProduct("Cherry");

    }
}
