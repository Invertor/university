package ru.kostrov.home7.unique_Chars;

import javafx.collections.transformation.SortedList;

import java.util.*;

public class UniqueChars {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void calculate() {
        String res="";
        TreeMap<Character,Integer> freq1=new TreeMap<>();
        for(Character ch:text.toUpperCase().toCharArray()){
            if(Character.isAlphabetic(ch)){
                if(freq1.containsKey(ch)){
                    freq1.replace(ch,freq1.get(ch)+1);
                }else{
                    freq1.put(ch,1);
                }
            }
        }
        TreeMap<Integer,String> freq2=new TreeMap<>();
        List<Integer> freq3=new ArrayList<>();
        for(Character ch:freq1.keySet()){
            Integer f;
            f=freq1.get(ch);
            if(freq2.containsKey(f)){
                String s;
                s=freq2.get(f);
                freq2.replace(f,s+ch);
            }else{
                freq2.put(f,ch.toString());
                freq3.add(f);
            }
        }
        Collections.sort(freq3);
        for(Integer f:freq3){
            String s;
            s=freq2.get(f);
            res=f.toString()+": "+s+" \r\n"+res;
        }
        text=res;
    }
}
