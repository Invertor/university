package ru.kostrov.home9.mathbox;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Collection;

public class MyProxy implements InvocationHandler {
    private MathBox testBox;
    MyProxy (MathBox testClass){testBox=testClass;}

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable{
        Method trueMethod;
        Field trueCollection;
        boolean log=false;
        boolean clear=false;
        trueMethod=testBox.getClass().getMethod(method.getName(),method.getParameterTypes());
        trueCollection=testBox.getClass().getDeclaredField("collection");
        for(Annotation ann:trueMethod.getAnnotations()){
            //System.out.println(ann.toString());
            if(ann.toString().endsWith("ClearData()")){
                clear=true;
            }
            if(ann.toString().endsWith("LogData()")){
                log=true;
            }
        }
        if(log){
            trueCollection.setAccessible(true);
            System.out.println(trueCollection.get(testBox).toString());
            trueCollection.setAccessible(false);
        }
        Object res= method.invoke(testBox, args);
        if(log){
            trueCollection.setAccessible(true);
            System.out.println(trueCollection.get(testBox).toString());
            trueCollection.setAccessible(false);
        }
        if(clear){
            trueCollection.setAccessible(true);
            ((Collection)trueCollection.get(testBox)).clear();
            trueCollection.setAccessible(false);
        }
        return res;
    }
}
