package ru.kostrov.home9.mathbox;

import java.util.*;

public class MathBox implements MathBoxInterface {

    private HashSet<Integer> collection;

    public MathBox(){
        collection = new HashSet<>();
    }

    public MathBox(int[] inputArray){
        collection = new HashSet<>();
        for(Integer value: inputArray){
            collection.add(value);
        }
    }

    @LogData
    public void add(int x){
        collection.add(x);
    }

    @LogData
    public void remove(int x){
        collection.remove(x);
    }


    public int getTotal(){
        int sum=0;
        for (Integer x : collection) {
            sum = sum + x;
        }
        return sum;
    }

    public int getAverage(){
        long avg=0;
        for (Integer x : collection) {
            avg = avg + x;
        }
        avg=avg/collection.size();
        return (int)avg;
    }

    @LogData
    public Collection<Integer> getMultiplied(int multiplier){
        ArrayList<Integer> myCollection1 = new ArrayList<>();
        for (Integer x : collection) {
            myCollection1.add(x * multiplier);
        }
        collection.clear();
        collection.addAll(myCollection1);
        return myCollection1;
    }

    public Map<Integer,String> getMap(){
        Map<Integer,String> myMap=new HashMap<>();
        for (Integer x : collection) {
            myMap.put(x, x.toString());
        }
        return myMap;
    }

    @ClearData
    public int getMax(){
        return Collections.max(collection);
    }

    @ClearData
    public int getMin(){
        return Collections.min(collection);
    }

    @Override
    public String toString(){
        return collection.toString();
    }

}
