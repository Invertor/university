package ru.kostrov.home9.mathbox;

import java.util.Collection;
import java.util.Map;

public interface MathBoxInterface {
    void add(int x);
    void remove(int x);
    int getTotal();
    int getAverage();
    Collection<Integer> getMultiplied(int multiplier);
    Map<Integer,String> getMap();
    int getMax();
    int getMin();
}
