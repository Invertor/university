package ru.kostrov.home9.mathbox;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        int initArray[]={1,3,5,7,4};
        MathBox collection =new MathBox(initArray);
        MyProxy proxy = new MyProxy(collection);
        MathBoxInterface myTestProxy = (MathBoxInterface) Proxy.newProxyInstance(
                ru.kostrov.logged.MyProxy.class.getClassLoader(),
                new Class[]{MathBoxInterface.class},
                proxy
        );
        myTestProxy.add(2);
        myTestProxy.remove(4);
        System.out.println(myTestProxy.getTotal());
        myTestProxy.getMultiplied(2);
        System.out.println(myTestProxy.getTotal());
        System.out.println(myTestProxy.getMax());
        System.out.println(myTestProxy.getMap());
    }
}
