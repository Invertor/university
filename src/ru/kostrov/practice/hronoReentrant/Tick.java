package ru.kostrov.practice.hronoReentrant;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Tick extends Thread{
    private ReentrantLock lock;
    private Condition cond;
    private Counter counter;
    private int timeOut;
    private long startms;
    ArrayList<Thread> managed;

    Tick(ReentrantLock lock, Condition cond, Counter counter, int timeOut, ArrayList<Thread> managed){
        this.counter=counter;
        this.lock=lock;
        this.cond=cond;
        this.timeOut=timeOut;
        startms=System.currentTimeMillis();
        this.managed=managed;
    }

    public void run(){
        while (! isInterrupted() ) {
            try {
                sleep(1000);
                lock.lockInterruptibly();
                counter.add();
                if (counter.get()>timeOut){
                    break;
                }
                cond.signalAll();
            } catch (InterruptedException e) {
                break;
            }finally {
                lock.unlock();
            }
            System.out.println(System.currentTimeMillis()-startms);
        }
        for(Thread th : managed) {
            th.interrupt();
        }
    }
}
