package ru.kostrov.practice.hronoReentrant;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    private static ArrayList<Thread> outs=new ArrayList<>();
    public static void main(String[] args) {
        Thread tick,out5,out7;
        ReentrantLock lock = new ReentrantLock();
        Condition cond = lock.newCondition();
        Counter cnt = new Counter();
        out5=new Out(lock, cond, cnt,5);
        out5.start();
        outs.add(out5);
        out7=new Out(lock, cond, cnt,7);
        out7.start();
        outs.add(out7);
        tick=new Tick(lock, cond, cnt,33, outs);
        tick.start();
    }

}
