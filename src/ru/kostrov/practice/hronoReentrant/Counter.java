package ru.kostrov.practice.hronoReentrant;

class Counter {
    int count;

    Counter(){
        count=0;
    }

    void add(){
        count++;
    }

    int get(){
        return count;
    }
}
