package ru.kostrov.practice.hronoReentrant;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Out extends Thread {
    private ReentrantLock lock;
    private Condition cond;
    private Counter counter;
    private int seconds;

    Out(ReentrantLock lock, Condition cond, Counter counter, int seconds){
        this.seconds=seconds;
        this.counter=counter;
        this.lock=lock;
        this.cond=cond;
    }

    public void run(){
        while(! isInterrupted()){
            try {
                lock.lockInterruptibly();
                do {
                    cond.await();
                }while(counter.get()%seconds!=0);
            } catch (InterruptedException e) {
                break;
            }finally {
                lock.unlock();
            }
            System.out.printf("%d секунд\r\n", seconds);
        }
    }
}
