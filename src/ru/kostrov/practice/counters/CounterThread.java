package ru.kostrov.practice.counters;

import java.util.concurrent.atomic.AtomicInteger;

public class CounterThread extends Thread {
    private AtomicInteger sum;
    CounterThread(AtomicInteger sum){
        this.sum=sum;
    }
    public void run(){
        int localSum;
        while (! this.isInterrupted()){
            localSum=sum.incrementAndGet();
            System.out.println(localSum);
            try {
                sleep(100);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
