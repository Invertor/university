package ru.kostrov.practice.counters;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.sleep;

public class Main {
    private static AtomicInteger sum=new AtomicInteger(0);
    public static void main(String[] args) {
        ArrayList<Thread> threadArray=new ArrayList<>();
        Thread newThread;

        for (int i = 0; i < 4; i++) {
            newThread=new CounterThread(sum);
            threadArray.add(newThread);
        }
        for (Thread stopThread: threadArray) {
            stopThread.start();
        }
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
        for (Thread stopThread: threadArray) {
            stopThread.interrupt();
        }
        for (Thread stopThread: threadArray) {
            try {
                stopThread.join();

            } catch (InterruptedException e) {
                System.out.println("Interrupted");
            }
        }
        System.out.printf("Результат: %d",sum.get());
    }
}
