package ru.kostrov.practice.threads;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Thread> threadArray=new ArrayList<>();
        Thread newThread;

        for (int i = 0; i < 10; i++) {
            newThread=new TestThread(i);
            newThread.start();
            threadArray.add(newThread);
        }
        for (Thread stopThread: threadArray) {
            try {
                stopThread.join();
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
            }
        }
    }
}
