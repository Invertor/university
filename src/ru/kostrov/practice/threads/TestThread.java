package ru.kostrov.practice.threads;

public class TestThread extends Thread{
    private int num;
    TestThread(int num){
        this.num=num;
    }
    public void run (){
        try{
            Thread.sleep(100);
            System.out.println(num);
        } catch (InterruptedException ex) {
            System.out.println("Interrupted");
        }
    }
}
