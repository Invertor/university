package ru.kostrov.practice.hrono;

import java.util.ArrayList;

public class Tick extends Thread{
    private final Object notifyObject;
    private int count, timeOut;
    private long startms;
    ArrayList<Thread> managed;

    Tick(Object notifyObject, int timeOut, ArrayList<Thread> managed){
        this.notifyObject=notifyObject;
        this.timeOut=timeOut;
        startms=System.currentTimeMillis();
        this.managed=managed;
        count=0;
    }

    public void run(){
        while (! isInterrupted() && count<timeOut) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
            count++;
            System.out.println(System.currentTimeMillis()-startms);
            synchronized (notifyObject) {
                notifyObject.notifyAll();
            }
        }
        for(Thread th : managed) {
            th.interrupt();
        }
    }
}
