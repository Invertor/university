package ru.kostrov.practice.hrono;

public class Out extends Thread {
    private int seconds, count;
    private final Object notifyObject;

    Out(Object notifyObject, int seconds){
        this.seconds=seconds;
        this.notifyObject=notifyObject;
        count=0;
    }

    public void run(){
        while(! isInterrupted()){
            try {
                synchronized (notifyObject) {
                    notifyObject.wait();
                }
            } catch (InterruptedException e) {
                break;
            }
            count++;
            if (count%seconds==0){
                System.out.printf("%d секунд\r\n", seconds);
            }
        }
    }
}
