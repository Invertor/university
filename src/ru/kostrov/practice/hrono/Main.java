package ru.kostrov.practice.hrono;

import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class Main {
    private static final Integer anchor=0;
    private static ArrayList<Thread> outs=new ArrayList<>();
    public static void main(String[] args) {
        Thread tick,out5,out7;
        out5=new Out(anchor,5);
        out5.start();
        outs.add(out5);
        out7=new Out(anchor,7);
        out7.start();
        outs.add(out7);
        tick=new Tick(anchor,33, outs);
        tick.start();
    }

}
