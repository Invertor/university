package ru.kostrov.logged;
import java.lang.annotation.*;
import java.lang.annotation.Retention;

@Retention( RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MyLog {
    boolean logged() default false;
}
