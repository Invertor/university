package ru.kostrov.logged;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Time;
import java.time.LocalDateTime;

public class MyProxy implements InvocationHandler {
    private TestClass tstC;
    public  MyProxy(TestClass tstCl){tstC=tstCl;}

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Method trueMethod;
        boolean logged=false;
        long time=0;
        trueMethod=tstC.getClass().getMethod(method.getName(),method.getParameterTypes());
        for(Annotation ann:trueMethod.getAnnotations()){
            //System.out.println(ann.toString());
            if(ann.toString().contains("(logged=true)")){
                logged=true;
            }
        }
        if(logged){
            System.out.println(LocalDateTime.now());
            time=System.currentTimeMillis();
        }
        Object res= method.invoke(tstC, args);
        if(logged){
            time=System.currentTimeMillis()-time;
            System.out.println(time);
        }
        return res;
    }
}
