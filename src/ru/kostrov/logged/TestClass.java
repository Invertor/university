package ru.kostrov.logged;


import ru.kostrov.reflection.MyInterface;

import static java.lang.Thread.sleep;

public class TestClass implements MyInterface {
    public TestClass(){

    }
    public void method1(){
        try {
            sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Test1");
    }
    @MyLog(logged=true)
    public void method2(){
        try {
            sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Test2");
    }
}
