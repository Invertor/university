package ru.kostrov.logged;

import ru.kostrov.reflection.MyInterface;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        TestClass tst = new TestClass();
        MyProxy myProxy = new MyProxy(tst);
        MyInterface myTstProxy = (MyInterface) Proxy.newProxyInstance(
                MyProxy.class.getClassLoader(),
                new Class[]{MyInterface.class},
                myProxy
        );
        System.out.println("Without proxy:");
        tst.method1();
        tst.method2();
        System.out.println("With proxy:");
        myTstProxy.method1();
        myTstProxy.method2();

    }
}
