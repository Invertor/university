package ru.kostrov.reflection;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        TestClass tstC=new TestClass();
        //Integer tstC=1;
        printAnnotations(myAnnotations(tstC));
        printAnnotationMap(myMethodAnnnotations(tstC));
    }

    private static ArrayList<Annotation> myAnnotations(Object tstC) {
        ArrayList<Annotation> annList = new ArrayList<>();
        for (Annotation ann : tstC.getClass().getAnnotations()) {
            annList.add(ann);
        }
        return annList;
    }

    private static HashMap<Method,ArrayList<Annotation>> myMethodAnnnotations(Object tstC){
        HashMap<Method,ArrayList<Annotation>> metList= new HashMap<>();
        for(Method met:tstC.getClass().getDeclaredMethods()){
            ArrayList<Annotation> annList = new ArrayList<>();
            for (Annotation ann : met.getAnnotations()) {
                annList.add(ann);
            }
            metList.put(met,annList);

        }
        return metList;
    }

    private static void printAnnotations(ArrayList<Annotation> annList){
        for(Annotation ann:annList){
            System.out.println(ann.toString());
        }
    }

    private static void printAnnotationMap(HashMap<Method,ArrayList<Annotation>> annMap) {
        for (Method met : annMap.keySet()) {
            System.out.println("Метод:"+met.getName());
            printAnnotations(annMap.get(met));
        }
    }

}
