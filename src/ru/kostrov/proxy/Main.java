package ru.kostrov.proxy;

import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        Trainer mikhail =
                new JavaTrainer();
        TrainingCenter trainingCenter =
                new TrainingCenter(mikhail);

        Trainer stc = (Trainer) Proxy.newProxyInstance(
                TrainingCenter.class.getClassLoader(),
                new Class[]{Trainer.class},
                trainingCenter);
        System.out.println("Without proxy:");
        mikhail.eat();
        mikhail.teach(10);
        System.out.println("With proxy:");
        stc.teach(10);
        stc.eat();
        stc.talk();
    }
}
