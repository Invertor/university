package ru.kostrov.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class TrainingCenter implements InvocationHandler {
    private Trainer trainer;

    public TrainingCenter(Trainer trainer) {
        this.trainer = trainer;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //method.getAnnotation()

        System.out.println("Вызван метод:"+method.getName());
        if(args!=null) {
            for (Object arg:args) {
                System.out.println("Параметр:"+arg.toString());
            }
        }
        /*if (method.getName().equals("talk")) {
            System.out.println("who's late");
        }
        if (method.getName().equals("eat")) {
            System.out.println("Robots don't eat");
        }*/
        return method.invoke(trainer, args);
    }
}
