package ru.kostrov.proxy;

public interface Trainer {
    public void teach(int min);

    public void eat();

    public void talk();
}
