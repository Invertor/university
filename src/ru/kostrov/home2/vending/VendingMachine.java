package ru.kostrov.home2.vending;

import ru.kostrov.home2.vending.drinks.Drink;

public class VendingMachine {
    private double money = 0;
    private Drink[] drinks;

    public VendingMachine() {
    }

    public VendingMachine(Drink[] drinks) {
        this.drinks = drinks;
    }

    public void addMoney(double money) {
        if (money > 0) {
            this.money += money;
            System.out.printf("Остаток: %.0f\n", this.money);
        }
    }

    private Drink getDrink(int key) {
        if (key < drinks.length) {
            return drinks[key];
        } else {
            return null;
        }
    }

    private Drink getDrink(String title) {
        for (Drink drink : drinks) {
            if (drink.getTitle().toLowerCase().compareTo(title.toLowerCase()) == 0) {
                return (drink);
            }
        }
        return null;
    }

    public void giveMeADrink(int key) {
        Drink drink = getDrink(key);
        giveMeADrink(drink);
    }

    private void giveMeADrink(Drink drink) {
        if (this.money > 0) {
            if (drink != null) {
                if (drink.getPrice() <= money) {
                    System.out.println("Возьмите ваш напиток: " + drink.getTitle());
                    money -= drink.getPrice();
                    System.out.printf("Остаток: %.0f\n", this.money);
                } else {
                    System.out.println("Недостаточно средств!");
                }
            }
        } else {
            System.out.println("Бесплатно не работаем!");
        }
    }


    public void giveMeADrink(String title) {
        Drink drink = getDrink(title);
        giveMeADrink(drink);
    }

    public void setDrinks(Drink[] drinks) {
        this.drinks = drinks;
    }

    public void priceList() {
        System.out.println("В наличии:");
        for (Drink drink : drinks) {
            System.out.printf("%s %.0f\n", drink.getTitle(), drink.getPrice());
        }
    }
}
