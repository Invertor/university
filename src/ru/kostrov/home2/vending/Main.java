package ru.kostrov.home2.vending;

import ru.kostrov.home2.vending.drinks.ColdDrink;
import ru.kostrov.home2.vending.drinks.Drink;
import ru.kostrov.home2.vending.drinks.HotDrink;

import ru.kostrov.home2.vending.drinks.HotDrinks;
import ru.kostrov.home2.vending.drinks.ColdDrinks;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Drink[] hotDrinks = new HotDrink[]{new HotDrink(HotDrinks.Tea, 150)};
        Drink[] coldDrinks = new ColdDrink[]{new ColdDrink(ColdDrinks.Cola, 50), new ColdDrink(ColdDrinks.Kvas, 80)};

        VendingMachine vm = new VendingMachine(hotDrinks);
        vm.priceList();
        Scanner userInput = new Scanner(System.in);
        do {
            if (userInput.hasNext()) {
                if (userInput.hasNextInt()) {
                    int typedNumber = userInput.nextInt();
                    if (typedNumber <= 0) {
                        break;
                    }
                    vm.addMoney(typedNumber);
                } else {
                    String typedWord = userInput.next();
                    if ("cold".compareTo(typedWord.toLowerCase()) == 0) {
                        vm.setDrinks(coldDrinks);
                        vm.priceList();
                    } else if ("hot".compareTo(typedWord.toLowerCase()) == 0) {
                        vm.setDrinks(hotDrinks);
                        vm.priceList();
                    } else if ("pricelist".compareTo(typedWord.toLowerCase()) == 0) {
                        vm.priceList();
                    } else if ("exit".compareTo(typedWord.toLowerCase()) == 0) {
                        break;
                    } else
                        vm.giveMeADrink(typedWord);
                }
            }
        } while (true);

        //vm.addMoney(200);
        //vm.giveMeADrink(0);

        //vm.setDrinks(coldDrinks);
        //vm.giveMeADrink(0);
    }
}