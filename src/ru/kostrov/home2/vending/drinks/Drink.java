package ru.kostrov.home2.vending.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}