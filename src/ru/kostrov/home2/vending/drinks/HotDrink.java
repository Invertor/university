package ru.kostrov.home2.vending.drinks;

public class HotDrink implements Drink {
    private String title;
    private double price;
    private HotDrinks drink;

    public HotDrink(String title, double price) {
        this.title = title;
        this.price = price;
    }

    public HotDrink(HotDrinks drink, double price ){
        this(drink.toString(),price);
        this.drink = drink;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String getTitle() {
        return title;
    }
}
