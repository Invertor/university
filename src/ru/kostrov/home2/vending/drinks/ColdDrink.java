package ru.kostrov.home2.vending.drinks;

public class ColdDrink implements Drink {
    private String title;
    private double price;
    private ColdDrinks drink;

    public ColdDrink(String title, double price) {
        this.title = title;
        this.price = price;
    }

    public ColdDrink(ColdDrinks drink, double price ){
        this(drink.toString(),price);
        this.drink = drink;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String getTitle() {
        return title;
    }
}