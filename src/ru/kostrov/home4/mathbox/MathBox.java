package ru.kostrov.home4.mathbox;

import java.util.HashMap;

public class MathBox {
    private HashMap<Double, Number> myCollection;
    public MathBox(Number[] inputArray){
        myCollection = new HashMap<>();
        for(Number num: inputArray){

            myCollection.put(num.doubleValue(),num);
        }
    }
    public double summator(){
        double sum=0;
        for(Double x: myCollection.keySet()){
            sum=sum+x;
        }
        return sum;
    }
    public void splitter(double divider){
        HashMap<Double, Number> myCollection1 = new HashMap<>();
        for(Double x: myCollection.keySet()){
            myCollection1.put(x/divider, x/divider);
        }
        myCollection = myCollection1;
    }

}
